package com.gildedrose.service;

import com.gildedrose.item.repository.ItemTypeRepository;
import com.gildedrose.service.exception.FileCorruptedException;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class CsvReaderTest {

    public static final String VALID_CSV_FILE = "itemtypes.csv";
    public static final String INVALID_CSV_FILE_MISSING_DELIMINATOR = "itemtypesmissingdeliminator.csv";
    public static final String INVALID_CSV_FILE_MULTIPLE_DELIMINATOR = "itemtypesmultipledeliminator.csv";
    public static final String INVALID_CSV_FILE_EMPTY_LINE = "itemtypesemptyline.csv";

    private CsvReader csvReader = new CsvReader();

    @Test
    public void testReadValidCsvFile() throws IOException, URISyntaxException {
        List<ItemTypeRepository> itemTypeRepositoryList = csvReader.readItemTypes(VALID_CSV_FILE);
        assertNotNull(itemTypeRepositoryList);
        assertEquals(4, itemTypeRepositoryList.size());
        assertEquals("OlderBetter", itemTypeRepositoryList.get(0).getType());

    }

    @Test(expected = FileCorruptedException.class)
    public void testReadInValidCsvFileWithNoDeliminator() throws IOException, URISyntaxException {
        csvReader.readItemTypes(INVALID_CSV_FILE_MISSING_DELIMINATOR);
    }

    @Test(expected = FileCorruptedException.class)
    public void testReadInValidCsvFileWithMultipleDeliminator() throws IOException, URISyntaxException {
        csvReader.readItemTypes(INVALID_CSV_FILE_MULTIPLE_DELIMINATOR);
    }

    @Test(expected = FileCorruptedException.class)
    public void testReadInValidCsvFileWithEmptyLine() throws IOException, URISyntaxException {
        csvReader.readItemTypes(INVALID_CSV_FILE_EMPTY_LINE);
    }



}