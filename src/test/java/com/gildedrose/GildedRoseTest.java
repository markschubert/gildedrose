package com.gildedrose;

import com.gildedrose.service.CsvReader;
import org.apache.log4j.Appender;
import org.apache.log4j.LogManager;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URISyntaxException;

import static com.gildedrose.GildedRose.ERROR_WHILE_READING_CSV_FILE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GildedRoseTest {

    @Mock
    private Appender mockAppender;

    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    private GildedRose gildedRose;


    @Before
    public void setup() {
        mockAppender = mock(Appender.class);
        MockitoAnnotations.initMocks(this);
        LogManager.getRootLogger().addAppender(mockAppender);
    }

    @After
    public void teardown() {
        LogManager.getRootLogger().removeAppender(mockAppender);
    }

    @Test
    public void testNormalItemSellInAndQualityDecreasesNextDay(){
        Item[] items = new Item[] { new Item("Pure chocolate", 5, 10)};
        gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(9, gildedRose.getItems()[0].quality);
        assertEquals(4, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testNormalItemSellInAndQualityDecreasesEveryDay(){
        Item[] items = new Item[] { new Item("Pure chocolate", 5, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(4);
        assertEquals(6, gildedRose.getItems()[0].quality);
        assertEquals(1, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testNormalItemSellInAndQualityDecreasesTwiceAsFastWhenExpired(){
        Item[] items = new Item[] { new Item("Pure chocolate", 5, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(6);
        assertEquals(3, gildedRose.getItems()[0].quality);
        assertEquals(-1, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testNormalItemQualityIsNeverNegative(){
        Item[] items = new Item[] { new Item("Pure chocolate", 5, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(15);
        assertEquals(0, gildedRose.getItems()[0].quality);
        assertEquals(-10, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testOlderBetterItemSellInDecreasesAndQualityIncreasesNextDay(){
        Item[] items = new Item[] { new Item("Aged Brie", 5, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQuality();
        assertEquals(11, gildedRose.getItems()[0].quality);
        assertEquals(4, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testOlderBetterItemSellInDecreasesAndQualityIncreasesEveryDay(){
        Item[] items = new Item[] { new Item("Aged Brie", 5, 10)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(4);
        assertEquals(14, gildedRose.getItems()[0].quality);
        assertEquals(1, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testOlderBetterItemQualityNotHigherThanLimit(){
        Item[] items = new Item[] { new Item("Aged Brie", 5, 48)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(4);
        assertEquals(50, gildedRose.getItems()[0].quality);
        assertEquals(1, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testOlderBetterItemQualityIncreasesDoubleAsFastAfterSellInDay(){
        Item[] items = new Item[] { new Item("Aged Brie", 5, 5)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(10);
        assertEquals(20, gildedRose.getItems()[0].quality);
        assertEquals(-5, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testLegendaryItemQualityAndSelInNeverChange(){
        Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 0, 80)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(4);
        assertEquals(80, gildedRose.getItems()[0].quality);
        assertEquals(0, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testTicketsIncreasesInQuality(){
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 5)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(15);
        assertEquals(35, gildedRose.getItems()[0].quality);
        assertEquals(0, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testTicketsIncreasesInQualityEvenMoreWhenPassingLimit(){
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 40)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(15);
        assertEquals(50, gildedRose.getItems()[0].quality);
        assertEquals(0, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testTicketsLostValueAfterConcertDay(){
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 40)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(16);
        assertEquals(0, gildedRose.getItems()[0].quality);
        assertEquals(-1, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testConjuredItemQualityDecreasesTwiceAsFastAsNormalItem(){
        Item[] items = new Item[] { new Item("Conjured", 5, 50)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(4);
        assertEquals(42, gildedRose.getItems()[0].quality);
        assertEquals(1, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testConjuredItemQualityDecreasesTwiceAsFastAsNormalItemWhenItemIsExpired(){
        Item[] items = new Item[] { new Item("Conjured", 5, 50)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.updateQualityForSpecificNumberOfDays(10);
        assertEquals(20, gildedRose.getItems()[0].quality);
        assertEquals(-5, gildedRose.getItems()[0].sellIn);
    }

    @Test
    public void testItemNotUpdatedWhenFailReadingCsvWithIOException() throws IOException, URISyntaxException {
        CsvReader csvReaderMock = mock(CsvReader.class);
        doThrow(IOException.class).when(csvReaderMock).readItemTypes(anyString());
        Item[] items = new Item[] { new Item("Conjured", 5, 50)};
        GildedRose gildedRose = new GildedRose(items);
        gildedRose.setCsvReader(csvReaderMock);
        gildedRose.updateQuality();
        assertEquals(5, items[0].sellIn);
        assertEquals(50, items[0].quality);

        assertErrorLogggedWhenReadingCsvFile();
    }

    @Test
    public void testItemNotUpdatedWhenFailReadingCsvWithURISyntaxException() throws IOException, URISyntaxException {
        CsvReader csvReaderMock = mock(CsvReader.class);
        doThrow(URISyntaxException.class).when(csvReaderMock).readItemTypes(anyString());
        Item[] items = new Item[] { new Item("Conjured", 5, 50)};
        gildedRose = new GildedRose(items);
        gildedRose.setCsvReader(csvReaderMock);
        gildedRose.updateQuality();
        assertEquals(5, items[0].sellIn);
        assertEquals(50, items[0].quality);

        assertErrorLogggedWhenReadingCsvFile();


    }

    @Test
    public void testPrintItem(){
        Item[] items = new Item[] { new Item("Conjured", 5, 50)};
        assertEquals(items[0].name + ", " + items[0].sellIn + ", " + items[0].quality, items[0].toString());

    }

    private void assertErrorLogggedWhenReadingCsvFile() {
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getValue();
        assertEquals(ERROR_WHILE_READING_CSV_FILE, loggingEvent.getRenderedMessage());
    }










}
