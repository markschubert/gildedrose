package com.gildedrose;

import com.gildedrose.item.repository.ItemTypeRepository;
import com.gildedrose.item.types.CustomizedItem;
import com.gildedrose.service.CsvReader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GildedRose {

    public static final Logger logger = LogManager.getLogger(GildedRose.class);
    private static final String CSV_FILE = "itemtypes.csv";
    private static final String TYPE_NORMAL = "Normal";
    public static final String ERROR_WHILE_READING_CSV_FILE = "Error while reading CSV file. Please ensure that itemtypes.csv exist in the resources folder.";

    private Item[] items;
    public GildedRose(Item[] items) {
        this.items = items;
    }
    private CsvReader csvReader = new CsvReader();

    public void updateQualityForSpecificNumberOfDays(int numberOfDays){
        IntStream.range(0, numberOfDays).forEach(day -> updateQuality());
    }

    public void updateQuality() {
        try {
            List<ItemTypeRepository> itemTypeRepositories = readItemTypesFromDataSource();
            updateQualityOfEachItem(itemTypeRepositories);
        } catch (IOException e) {
            logErrorForFailureInReadingFile();
        } catch (URISyntaxException e) {
            logErrorForFailureInReadingFile();
        }
    }

    private void updateQualityOfEachItem(List<ItemTypeRepository> itemTypeRepositories) {
        for (Item currentItem : items) {
            String type = getItemTypeFromItemTypeRepository(itemTypeRepositories, currentItem);
            CustomizedItem customizedItem = CustomizedItem.createCustomizedItem(type);
            customizedItem.updateItem(currentItem);
        }
    }

    private String getItemTypeFromItemTypeRepository(List<ItemTypeRepository> itemTypeRepositories, Item currentItem) {
        List<ItemTypeRepository> itemTypeRepositoryList = itemTypeRepositories.stream().filter(item -> item.getName().equalsIgnoreCase(currentItem.name)).collect(Collectors.toList());
        String type;
        if(itemTypeRepositoryList.size() > 0) {
            type = itemTypeRepositoryList.get(0).getType();
        } else{
            type = TYPE_NORMAL;
        }
        return type;
    }

    public List<ItemTypeRepository> readItemTypesFromDataSource() throws IOException, URISyntaxException {
        List<ItemTypeRepository> itemTypeRepositories = csvReader.readItemTypes(CSV_FILE);
        return itemTypeRepositories;
    }

    private void logErrorForFailureInReadingFile() {
        logger.error(ERROR_WHILE_READING_CSV_FILE);
    }

    public Item[] getItems() {
        return items;
    }

    public void setCsvReader(CsvReader csvReader) {
        this.csvReader = csvReader;
    }

}