package com.gildedrose.service;

import com.gildedrose.item.repository.ItemTypeRepository;
import com.gildedrose.service.exception.FileCorruptedException;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CsvReader {

    public static final String TYPE_HEADER_LABEL = "Type";
    public static final String NAME_HEADER_LABEL = "Name";

    public List<ItemTypeRepository> readItemTypes(String file) throws IOException, URISyntaxException {

        URL resource = getClass().getClassLoader().getResource(file);
        Stream<String> streamOfLines = Files.lines(Paths.get(resource.toURI()));
        List<ItemTypeRepository> itemTypeRepositoryList = constructItemTypeRepositoryList(streamOfLines);

        return itemTypeRepositoryList;
    }

    private List<ItemTypeRepository> constructItemTypeRepositoryList(Stream<String> streamOfLines) {
        return streamOfLines
                .map(this::constructItemRepositories)
                .filter(item -> isNotHeaderOfTheCsvFile(item))
                .collect(Collectors.toList());
    }

    private ItemTypeRepository constructItemTypeRepository(String[] words) {
        ItemTypeRepository itemTypeRepository = new ItemTypeRepository();
        itemTypeRepository.setType(words[0]);
        itemTypeRepository.setName(words[1]);
        return itemTypeRepository;
    }

    private boolean isNotHeaderOfTheCsvFile(ItemTypeRepository item) {
        return !item.getType().equalsIgnoreCase(TYPE_HEADER_LABEL) && !item.getName().equalsIgnoreCase(NAME_HEADER_LABEL);
    }


    private ItemTypeRepository constructItemRepositories(String line) {
        validateLineInFile(line);
        String[] words = line.split("\\|");
        ItemTypeRepository itemTypeRepository = constructItemTypeRepository(words);
        return itemTypeRepository;
    }

    private void validateLineInFile(String line) {
        if(line == null || line.trim().equalsIgnoreCase("")){
            throw new FileCorruptedException("CSV File Corrupted: A line should not be empty");
        }
        if (!line.contains("|")) {
            throw new FileCorruptedException("CSV File Corrupted: A line is missing the deliminator |");
        }
        if(Arrays.stream(line.split("")).filter(letter -> letter.equals("|")).count() > 1){
            throw new FileCorruptedException("CSV File Corrupted: A line is contains more than 1 deliminator |");
        }
    }
}
