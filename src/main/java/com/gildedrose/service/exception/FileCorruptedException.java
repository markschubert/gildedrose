package com.gildedrose.service.exception;

public class FileCorruptedException extends RuntimeException {

    public FileCorruptedException(String message) {
        super(message);
    }
}
