package com.gildedrose.item.types;

import com.gildedrose.Item;

public class Normal extends CustomizedItem {

    @Override
    protected void updateQuality(Item item) {
        decreaseQualityWhenAboveMinQualityLimit(item);
    }

    @Override
    protected void updateQualityWhenItemIsExpired(Item item) {
        if (itemIsExpired(item)) {
            decreaseQualityWhenAboveMinQualityLimit(item);
        }
    }
}
