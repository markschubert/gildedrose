package com.gildedrose.item.types;

import com.gildedrose.Item;

public class Tickets extends CustomizedItem {

    @Override
    protected void updateQuality(Item item) {
        increaseQuantityWhenBelowMaxLimit(item);
        if (item.sellIn < 11) {
            increaseQuantityWhenBelowMaxLimit(item);
        }
        if (item.sellIn < 6) {
            increaseQuantityWhenBelowMaxLimit(item);
        }
    }

    @Override
    protected void updateQualityWhenItemIsExpired(Item item) {
        if (itemIsExpired(item)) {
            item.quality = 0;
        }
    }

    @Override
    public void updateItem(Item item){
        updateQuality(item);
        updateSellIn(item);
        updateQualityWhenItemIsExpired(item);
    }
}
