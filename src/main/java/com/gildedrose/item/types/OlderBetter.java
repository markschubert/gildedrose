package com.gildedrose.item.types;

import com.gildedrose.Item;


public class OlderBetter extends CustomizedItem {


    @Override
    protected void updateQualityWhenItemIsExpired(Item item) {
        if (itemIsExpired(item)) {
            increaseQuantityWhenBelowMaxLimit(item);
        }
    }

    @Override
    protected void updateQuality(Item item) {
        increaseQuantityWhenBelowMaxLimit(item);
    }

}
