package com.gildedrose.item.types;

public enum ItemTypesEnum {

    LEGENDARY("Legendary"),
    CONJURED("Conjured"),
    OLDER_BETTER("OlderBetter"),
    TICKETS("Tickets");

    private final String value;

    ItemTypesEnum(String value) {
        this.value = value;
    }


    public static boolean isLegendaryItem(String itemType){
        return itemType.equalsIgnoreCase(LEGENDARY.value);
    }

    public static boolean isOlderBetterItem(String itemType){
        return itemType.equalsIgnoreCase(OLDER_BETTER.value);
    }

    public static boolean isTickets(String itemType){
        return itemType.equalsIgnoreCase(TICKETS.value);
    }

    public static boolean isConjuredItem(String itemType){
        return itemType.equalsIgnoreCase(CONJURED.value);
    }


}
