package com.gildedrose.item.types;

import com.gildedrose.Item;

public abstract class CustomizedItem {

    public static final int MIN_QUALITY_LIMIT = 0;
    public static final int MAX_QUALITY = 50;

    CustomizedItem() {
    }

    public static CustomizedItem createCustomizedItem(String itemType) {
        CustomizedItem customizedItem;
        if (ItemTypesEnum.isOlderBetterItem(itemType)) {
            customizedItem = new OlderBetter();
        } else if (ItemTypesEnum.isTickets(itemType)) {
            customizedItem = new Tickets();
        } else if (ItemTypesEnum.isConjuredItem(itemType)) {
            customizedItem = new Conjured();
        } else if(ItemTypesEnum.isLegendaryItem(itemType)) {
            customizedItem = new Legendary();
        } else{
            customizedItem = new Normal();
        }

        return customizedItem;
    }

    protected abstract void updateQuality(Item item);
    protected abstract void updateQualityWhenItemIsExpired(Item item);

    public void updateItem(Item item){
        updateSellIn(item);
        updateQuality(item);
        updateQualityWhenItemIsExpired(item);
    }

    protected void updateSellIn(Item item) {
        item.sellIn = item.sellIn - 1;
    }

    protected boolean itemIsExpired(Item item) {
        return item.sellIn < 0;
    }

    protected void increaseQuantityWhenBelowMaxLimit(Item item) {
        if (item.quality < MAX_QUALITY) {
            item.quality = item.quality + 1;
        }
    }

    protected void decreaseQualityWhenAboveMinQualityLimit(Item item) {
        if (item.quality > MIN_QUALITY_LIMIT) {
            item.quality = item.quality - 1;
        }
    }


}
