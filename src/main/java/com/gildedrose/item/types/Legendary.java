package com.gildedrose.item.types;

import com.gildedrose.Item;

public class Legendary extends CustomizedItem {

    @Override
    protected void updateQuality(Item item) {
        //do nothing for Legendary Item
    }

    @Override
    protected void updateQualityWhenItemIsExpired(Item item) {
        //do nothing for Legendary Item
    }

    @Override
    protected void updateSellIn(Item item) {
        //do nothing for Legendary Item
    }
}
