Gilded Rose

This project is created to refactor the Gilded Rose application. The requirements of the application can be 
found on https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements.txt

The code has been refactored to:

1. Simplify
2. Improve readibility
3. Improve code reusability
4. Have full test coverage by unit test  
5. Create flexibility to add new items and item types. 

One of the big issue that the existing code has, is that new products of an existing 
type can not be added without changing the code. 
To solve this a data source in a form of CSV file has been created.

The file is located in the resources directory: itemtypes.csv.
The file uses "|" as a delimitor. New items can be added into this file. Also new item types.

Polymorphism has also been applied in the refactoring process. Adding a new item type can now be done by adding
a new subclass of "CustomizedItem.java" with its own update quality logic. This allows the existing code to remain
as it is.

To ensure the coverage of the added unit test, Pitest has been added as a tool to measure the line and mutation coverage.
The Pitest can be executed by running the command below on the project root folder:

mvn clean test org.pitest:pitest-maven:mutationCoverage

The reports of the pitest can be found in the target/pit-reports directory.
The reports shows that the line and mutation coverage is at 100%
 
 